using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxR : MonoBehaviour
{
    public float moveSpeed = 2.0f; // �������� ���� ������

    void Update()
    {
        // ���������� ���� ������� ��'����
        Vector3 newPosition = transform.position;
        newPosition.x += moveSpeed * Time.deltaTime; // ������ ��'��� ������

        // ��������, �� ��'��� ����� ���� ������
        if (newPosition.x > Camera.main.ViewportToWorldPoint(Vector3.one).x)
        {
            // ���� ��'��� ������ �� ��� ������ ������, ����������� ���� �����
            newPosition.x = Camera.main.ViewportToWorldPoint(Vector3.zero).x;
        }

        transform.position = newPosition; // ������������ ���� ������� ��'����
    }
}
