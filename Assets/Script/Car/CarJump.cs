using UnityEngine;

public class CarJump : MonoBehaviour
{
    public float jumpForce = 10f;
    public Move move;
    private Rigidbody2D rb;
    public GameObject[] tags;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X) )
        {
            Jump();
        }
    }

    void Jump()
    {
        bool canJump = CheckIfPlayerOnGround(); // ����������, �� ����� ������ �������

        if (canJump)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
 
            Invoke("SetGroundedTrue", 0.1f);
        }
    }

    bool CheckIfPlayerOnGround()
    {
        //Collider2D[] grounds = Physics2D.OverlapCollider(tags[0].GetComponent<PolygonCollider2D>().points, tags[0].transform.rotation);

        Collider2D[] wheels1 = Physics2D.OverlapCircleAll(tags[1].transform.position, 0.2f);
        Collider2D[] wheels2 = Physics2D.OverlapCircleAll(tags[2].transform.position, 0.2f);

        foreach (Collider2D wheelCollider in wheels1)
        {
            if (wheelCollider.CompareTag("Ground"))
            {
                return true;
            }
        }

        foreach (Collider2D wheelCollider in wheels2)
        {
            if (wheelCollider.CompareTag("Ground"))
            {
                return true;
            }
        }

        return false;
    }

  

    
}
