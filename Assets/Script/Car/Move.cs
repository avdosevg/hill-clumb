using UnityEngine;

public class Move : MonoBehaviour
{
    // ���������� ����� ��� ���� �� ��
    public KeyCode moveRight = KeyCode.D; // ������ ��� ���� ������
    public KeyCode smoothStop = KeyCode.Z; // ������ ��� ������ �������
    public KeyCode moveLeft = KeyCode.A; // ������ ��� ���� ����
    public KeyCode stop = KeyCode.Space; // ������ ��� ������� �������

    // ������������ ������ ����
    public float moveSpeed = 250; // �������� ����
    public bool invert = true; // ��������� ��� ���� �������� ����
    public float smoothStep = 1; // ��������� ����
    public float stopStep = 10; // ��������� ������� �������
    public float freeStep = 0.05f; // ��������� ������� ����

    // ����� ��� ������ ������ ���������
    public float tiltForceRight = 100f; // ���� ������ ������
    public float tiltForceLeft = 100f; // ���� ������ ����

    // �������� �����
    private JointMotor2D jointMotor = new JointMotor2D();
    private WheelJoint2D[] wheel;
    private Rigidbody2D body;
    private int inv;
    private float curSpeed, curSmooth;
    private float velocity;

    // ��������� �� ����� ��������� ��� ������
    public Transform carBody;

    void Awake()
    {
        // ������������ ���������� ���������� �� ����������� ����
        body = GetComponent<Rigidbody2D>();
        wheel = GetComponents<WheelJoint2D>();
        jointMotor.maxMotorTorque = 10000;
        jointMotor.motorSpeed = 0;

        // ����������� ������� ��� �������
        foreach (WheelJoint2D w in wheel)
        {
            w.useMotor = true;
            w.motor = jointMotor;
        }
    }

    // ������� ��� ���������� �������� �� ������� ����������� �������
    float Round(float value, float to)
    {
        return ((int)(value * to)) / to;
    }

    // ��������, �� ��������� �� ����
    public bool CheckIfGrounded()
    {
        float raycastDistance = 0.9f;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, raycastDistance);

        if (hit.collider != null)
        {
            return true;
        }
        else
        {
            // ������������ ������ ��������� �� ����, ���� �� �� �� ����
            if (Input.GetKey(moveRight))
            {
                curSpeed = moveSpeed * inv;
                curSmooth = smoothStep;

                carBody.Rotate(Vector3.forward, -tiltForceRight * Time.deltaTime);
            }
            else if (Input.GetKey(moveLeft))
            {
                curSpeed = -moveSpeed * inv;
                curSmooth = smoothStep;
                carBody.Rotate(Vector3.forward, tiltForceLeft * Time.deltaTime);
            }
            else
            {
                curSpeed = 0;
                curSmooth = freeStep;
            }

            return false;
        }
    }

    // ��������� �����
    void MovementControl()
    {
        bool isGrounded = CheckIfGrounded();

        if (isGrounded)
        {
            if (Input.GetKey(moveRight))
            {
                curSpeed = moveSpeed * inv;
                curSmooth = smoothStep;
            }
            else if (Input.GetKey(moveLeft))
            {
                curSpeed = -moveSpeed * inv;
                curSmooth = smoothStep;
            }
            else if (Input.GetKey(stop))
            {
                curSpeed = 0;
                curSmooth = stopStep;
            }
            else
            {
                // ��� ���������� ������ �������� �������� ������
                curSpeed = Mathf.Lerp(curSpeed, 0f, curSmooth * Time.deltaTime);
                curSmooth = freeStep;
            }
        }
    }
    void Update()
    {
        velocity = Round(body.velocity.x, 100f);
        if (invert) inv = -1; else if (!invert) inv = 1;

        MovementControl();
        AdjustSpeed();
    }

    // ������������ ��������
    void AdjustSpeed()
    {
        jointMotor.motorSpeed = Mathf.Lerp(jointMotor.motorSpeed, curSpeed, curSmooth * Time.deltaTime);
        foreach (WheelJoint2D w in wheel)
        {
            w.motor = jointMotor;
        }
    }
}
