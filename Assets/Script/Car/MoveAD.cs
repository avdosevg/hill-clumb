using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAD : MonoBehaviour
{
    public float speed = 5.0f; 

    void Update()
    {
       
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
     
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
    }
}